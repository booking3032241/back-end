﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookingApi.Models
{
    public class User : BaseModel
    {
        [Key]
        public int? id { get; set; }
        public string username { get; set; } = string.Empty;
        public string password { get; set; } = string.Empty;
        public string? email { get; set; }
        public string? role { get; set; }

        [NotMapped]
        public string? defaultImageSrc { get; set; }
    }
}
