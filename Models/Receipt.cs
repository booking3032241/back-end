using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BookingApi.Models;

namespace BookingApi.Models
{
    public class Receipt : BaseModel
    {
        [Key]
        public int? id { get; set; }
        public int? reservationId { get; set; }
        public int? estateId { get; set; }
        public int? paymentId { get; set; }
        public double totalPrice { get; set; }

        public virtual Payment? Payment { get; set; }
        public virtual Reservation? Reservation { get; set; }
        public virtual Estate? Estate { get; set; }
    }
}