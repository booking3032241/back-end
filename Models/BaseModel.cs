﻿using Azure;
using BookingApi.Data;
using BookingApi.Services;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing;

namespace BookingApi.Models
{
    public abstract class BaseModel
    {
        public int status { get; set; } = 1;
        public DateTime? createdAt { get; set; }
        public DateTime? updatedAt { get; set; }
    }
}
