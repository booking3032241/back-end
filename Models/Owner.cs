﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BookingApi.Models
{
    public class Owner : BaseModel
    {
        [Key]
        public int id { get; set; }
        public virtual int? userId { get; set; }

        [Required]
        [Column(TypeName = "varchar(250)")]
        public string firstname { get; set; } = string.Empty;

        [Required]
        [Column(TypeName = "varchar(250)")]
        public string lastname { get; set; } = string.Empty;

        [Column(TypeName = "varchar(5000)")]
        public string description { get; set; } = string.Empty;

        [Column(TypeName = "varchar(250)")]
        public string email { get; set; } = string.Empty;

        public string? phone { get; set; }

        [Column(TypeName = "varchar(250)")]
        public string address { get; set; } = string.Empty;

        [Column(TypeName = "varchar(250)")]
        public string city { get; set; } = string.Empty;

        [Column(TypeName = "varchar(250)")]
        public string state { get; set; } = string.Empty;

        [Column(TypeName = "varchar(250)")]
        public string zipcode { get; set; } = string.Empty;

        [DataType(DataType.Date)]
        public DateTime? birthDate { get; set; }

        [NotMapped]
        public string? defaultImageSrc { get; set; }

        [ForeignKey("userId")]
        public virtual User? User { get; set; }

    }
}
