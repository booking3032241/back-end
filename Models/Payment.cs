using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookingApi.Models
{
    public class Payment : BaseModel
    {
        [Key]
        public int id { get; set; }
        public int reservationId { get; set; }
        public double totalAmount { get; set; }
        public DateTime paymentDate { get; set; }

        public virtual Receipt? Receipt { get; set; }
        public virtual Reservation? Reservation { get; set; }

    }
}