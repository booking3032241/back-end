﻿using Azure;
using BookingApi.Data;
using BookingApi.Services;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing;

namespace BookingApi.Models
{
    public class Estate : BaseModel
    {
        [Key]
        public int id { get; set; }

        [Required]
        public string name { get; set; } = string.Empty;

        public string? description { get; set; }

        [Required]
        public int? nature { get; set; }

        public int? ptype { get; set; }

        public int? price { get; set; }

        public int? space { get; set; }

        public int? bedrooms { get; set; }

        public int? bathrooms { get; set; }

        public int? garages { get; set; }

        public string? address { get; set; }

        public string? country { get; set; }
        public string? city { get; set; }

        public string? state { get; set; }

        public string? zipcode { get; set; }
        public string? latitude { get; set; }
        public string? longitude { get; set; }
        
        public virtual int? ownerId { get; set; }

        [ForeignKey("ownerId")]
        public virtual Owner? owner { get; set; }

        [NotMapped]
        public string? imageSrc { get; set; }
        
        [NotMapped]
        public string? defaultImageSrc { get; set; }

        public List<Gallery>? galleries { get; set; }
    }
}
