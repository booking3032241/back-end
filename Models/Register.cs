﻿using System.ComponentModel.DataAnnotations;

namespace BookingApi.Models
{
    public class Register
    {
        [Required]
        public string? firstname { get; set; }

        [Required]
        public string? lastname { get; set; }

        public string? email { get; set; }


        public string? phone { get; set; }


        public DateTime? birthDate { get; set; }

        [Required]
        public string? username { get; set; }

        [Required]
        public string? password { get; set; }

        public string? role { get; set; }

    }
}
