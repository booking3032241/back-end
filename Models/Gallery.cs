﻿using BookingApi.Data;
using BookingApi.Services;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookingApi.Models
{
    public class Gallery : BaseModel
    {
        // entityId : {1 : User,  2 : Estate}
        [Key]
        public int id { get; set; }

        public string? description { get; set; }

        public int entityId { get; set; }

        public int entityIdentifier { get; set; }


        [Column(TypeName = "nvarchar(100)")]
        public string? imageName { get; set; }

        [Required]
        [NotMapped]
        public IFormFile imageFile { get; set; }

        [NotMapped]
        public string? imageSrc { get; set; }
    }
}
