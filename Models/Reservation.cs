using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookingApi.Models
{
    public class Reservation : BaseModel
    {
        [Key]
        public int? id { get; set; }

        [Required]
        public DateTime start { get; set; }

        [Required]
        public DateTime end { get; set; }

        public string? title { get; set; }
        public double total { get; set; }
        public int estateId { get; set; }
        public int ownerId { get; set; }

        [ForeignKey("ownerId")]
        public virtual Owner? Owner { get; set; }

        [ForeignKey("estateId")]
        public virtual Estate? Estate { get; set; }
        public virtual Payment? Payment { get; set; }
        public virtual Receipt? Receipt { get; set; }
    }
}