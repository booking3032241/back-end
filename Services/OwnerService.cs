﻿using static BookingApi.Services.GalleryService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using BookingApi.Models;
using Azure.Core;
using Microsoft.EntityFrameworkCore;
using BookingApi.Data;

namespace BookingApi.Services
{
    public interface IOwnerService
    {
        Task<Owner> GetOwner(int? ownerId);
        Task<Owner> GetOwnerByUser(int userId);
    }
    public class OwnerService : IOwnerService
    {
        private readonly ApplicationDbContext _context;
        private readonly IGalleryService _galleryService;

        public OwnerService(ApplicationDbContext context, IGalleryService galleryService)
        {
            _context = context;
            _galleryService = galleryService;
        }

        public async Task<Owner> GetOwnerByUser(int userId)
        {
            var owner = await _context.Owners.FirstOrDefaultAsync(m => m.userId == userId);

            if (owner != null)
            {
                owner.defaultImageSrc = _galleryService.GetDefaultImageSrc(1, userId);
            }

            return owner;
        }

        public async Task<Owner> GetOwner(int? id)
        {
            var owner = await _context.Owners.FirstOrDefaultAsync(m => m.id == id);

            if (owner != null)
            {
                owner.defaultImageSrc = _galleryService.GetDefaultImageSrc(1, id);
            }

            return owner;
        }

    }
}
