﻿using static BookingApi.Services.GalleryService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using BookingApi.Models;
using Azure.Core;
using Microsoft.EntityFrameworkCore;
using BookingApi.Data;

namespace BookingApi.Services
{
    public interface IEstateService
    {
        Task<List<Estate>> GetAllEstate();
        Task<Estate> GetEstate(int id);
    }

    public class EstateService : IEstateService
    {
        private readonly ApplicationDbContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IGalleryService _galleryService;
        private readonly IOwnerService _ownerService;

        public EstateService(ApplicationDbContext context, IHttpContextAccessor httpContextAccessor, IGalleryService galleryService, IOwnerService ownerService)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _galleryService = galleryService;
            _ownerService = ownerService;
        }

        public async Task<List<Estate>> GetAllEstate()
        {
            var list = new List<Estate>();
            var estates = await _context.Estates.Select(x => new Estate()
            {
                id = x.id,
                name = x.name,
                description = x.description,
                nature = x.nature,
                ptype = x.ptype,
                price = x.price,
                space = x.space,
                bedrooms = x.bedrooms,
                bathrooms = x.bathrooms,
                garages = x.garages,
                address = x.address,
                country = x.country,
                city = x.city,
                state = x.state,
                zipcode = x.zipcode,
                latitude = x.latitude,
                longitude = x.longitude,

            }).ToListAsync();

            if (estates?.Any() == true)
            {
                foreach (var item in estates)
                {
                    list.Add(new Estate()
                    {
                        id = item.id,
                        name = item.name,
                        description = item.description,
                        nature = item.nature,
                        ptype = item.ptype,
                        price = item.price,
                        space = item.space,
                        bedrooms = item.bedrooms,
                        bathrooms = item.bathrooms,
                        garages = item.garages,
                        address = item.address,
                        country = item.country,
                        city = item.city,
                        state = item.state,
                        zipcode = item.zipcode,
                        latitude = item.latitude,
                        longitude = item.longitude,
                        defaultImageSrc = _galleryService.GetDefaultImageSrc(2, item.id),
                        galleries = _galleryService.GetAllGallery(2, item.id)
                    }
                    ); ;
                }
            }

            return list;
        }
        public async Task<Estate> GetEstate(int id)
        {
            var estate = await _context.Estates.FirstOrDefaultAsync(m => m.id == id);

            if (estate != null)
            {
                estate.defaultImageSrc = _galleryService.GetDefaultImageSrc(2, id);
                estate.galleries = _galleryService.GetAllGallery(2, id);
                estate.owner = await _ownerService.GetOwner(estate.ownerId);
            }

            return estate;
        }

    }
}
