﻿using static BookingApi.Services.GalleryService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using BookingApi.Models;
using Azure.Core;
using Microsoft.EntityFrameworkCore;
using BookingApi.Data;

namespace BookingApi.Services
{
    public interface IGalleryService
    {
        string GetDefaultImageSrc(int entityId, int? entityIdentifier);
        string getImageSrc(string imageName);
        List<Gallery> GetAllGallery(int entityId, int entityIdentifier);
        Task<Gallery> GetDefaultGallery(int entityId, int entityIdentifier);
    }
    public class GalleryService : IGalleryService
    {
        private readonly ApplicationDbContext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public GalleryService(ApplicationDbContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<Gallery> GetDefaultGallery(int entityId, int entityIdentifier)
        {
            var imageName = string.Empty;
            var imageSrc = string.Empty;
            if (_context is not null)
            {
                var gallery = await _context.Galleries.FirstOrDefaultAsync(x => x.entityId == entityId && x.entityIdentifier == entityIdentifier);
                imageName = gallery?.imageName;
                imageSrc = getImageSrc(imageName);

                gallery.imageSrc = imageSrc;

                return gallery;
            }
            return null;

        }
        public string GetDefaultImageSrc(int entityId, int? entityIdentifier)
        {
            var imageName = string.Empty;
            var imageSrc = string.Empty;

            if (_context is not null)
            {
                imageName = _context.Galleries.FirstOrDefault(x => x.entityId == entityId && x.entityIdentifier == entityIdentifier)?.imageName;
                imageSrc = getImageSrc(imageName);

            }
            return imageSrc;
        }
        public List<Gallery> GetAllGallery(int entityId, int entityIdentifier)
        {
            var Scheme = _httpContextAccessor.HttpContext?.Request.Scheme;
            var Host = _httpContextAccessor.HttpContext?.Request.Host;
            var PathBase = _httpContextAccessor.HttpContext?.Request.PathBase;

            if (_context is not null)
            {
                var data = _context.Galleries
               .Select(x => new Gallery()
               {
                   id = x.id,
                   description = x.description,
                   entityId = x.entityId,
                   entityIdentifier = x.entityIdentifier,
                   imageName = x.imageName,
                   imageSrc = String.Format("{0}://{1}{2}/Images/{3}", Scheme, Host, PathBase, x.imageName)
               })
               .Where(x => x.entityId == entityId && x.entityIdentifier == entityIdentifier)
               .ToList();

                return data;
            }
            return null;
        }

        public string getImageSrc(string imageName)
        {
            var Scheme = _httpContextAccessor.HttpContext?.Request.Scheme;
            var Host = _httpContextAccessor.HttpContext?.Request.Host;
            var PathBase = _httpContextAccessor.HttpContext?.Request.PathBase;

            var imageSrc = String.Format("{0}://{1}{2}/Images/{3}", Scheme, Host, PathBase, imageName);

            return imageSrc;
        }
    }
}
