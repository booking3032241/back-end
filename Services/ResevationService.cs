using System.Collections.Generic;
using System.Linq;
using BookingApi.Models;
using System;
using BookingApi.Data;

namespace BookingApi.Services
{
    public interface IReservationService
    {
        List<Reservation> GetReservationsByEstateId(int estateId);
        void CreateReservation(Reservation reservation);
        List<Reservation> GetReservationById(int id);
        List<Reservation> SearchReservations(string title);
        bool checkValidatedDate(int estatedId, DateTime date);

    }
    public class ReservationService : IReservationService
    {

        private readonly ApplicationDbContext _context;

        public ReservationService(ApplicationDbContext dbContext)
        {
            _context = dbContext;
        }

        public List<Reservation> GetReservationsByEstateId(int estateId)
        {

            var reservations = (from r in _context.Reservations
                                where r.estateId == estateId
                                select new Reservation
                                {
                                    id = r.id,
                                    title = r.title,
                                    start = r.start,
                                    end = r.end,
                                    estateId = r.estateId,
                                    total = r.total,
                                    ownerId = r.ownerId
                                }).ToList();


            return reservations;
        }

        public void CreateReservation(Reservation reservation)
        {
            Reservation reservationDTO = new Reservation();
            reservationDTO.id = reservation.id;
            reservationDTO.start = reservation.start;
            reservationDTO.end = reservation.end;
            reservationDTO.title = reservation.title;
            reservationDTO.estateId = reservation.estateId;
            reservationDTO.total = reservation.total;
            reservationDTO.ownerId = reservation.ownerId;

            _context.Reservations.Add(reservationDTO);
            _context.SaveChanges();

            // Get newly created reservationId
            var reservationId = reservationDTO.id;

            DateTime paymentDate = DateTime.Now;

            Payment paymentDTO = new Payment();
            paymentDTO.reservationId = (int)reservationId;
            paymentDTO.totalAmount = reservation.total;
            paymentDTO.paymentDate = paymentDate;

            _context.Payments.Add(paymentDTO);
            _context.SaveChanges();

            // Get newly created paymentId
            var paymentId = paymentDTO.id;

            // Save receipt obj to DB
            Receipt receipt = new Receipt();
            receipt.paymentId = (int)paymentId;
            receipt.reservationId = (int)reservationId;
            receipt.estateId = reservation.estateId;
            receipt.totalPrice = reservation.total;

            _context.Receipts.Add(receipt);
            _context.SaveChanges();
        }

        /*
         This method is intended to return a list of events by Angular Bootstrap Reservation 6+
         in order to display the event.
        */
        public List<Reservation> GetReservationById(int id)
        {
            var reservation = (from r in _context.Reservations
                               join f in _context.Estates on r.estateId equals f.id
                               where r.id == id
                               select new Reservation
                               {
                                   id = (int)r.id,
                                   title = r.title,
                                   start = r.start,
                                   end = r.end,
                                   total = r.total,
                               }).ToList();
            return reservation;
        }

        public List<Reservation> SearchReservations(string title)
        {

            var reservations = (from r in _context.Reservations
                                where r.title.ToLower().Contains(title.ToLower())
                                select new Reservation
                                {
                                    id = r.id,
                                    title = r.title,
                                    start = r.start,
                                    end = r.end,
                                    estateId = r.estateId,
                                    total = r.total,
                                    ownerId = r.ownerId
                                }).ToList();

            return reservations;
        }

        public bool checkValidatedDate(int estatedId, DateTime date)
        {
            var reservations = _context.Reservations.Where(x => x.estateId == estatedId).Select(x => new Reservation()
            {
                start = x.start,
                end = x.end

            }).ToList();

            var isValidated = true;
            foreach (var item in reservations)
            {
                var start = item.start;
                var end = item.end;

                DateRange range = new DateRange(start, end);
                bool included = range.Includes(date);
                if (included)
                {
                    isValidated = false;
                }
            }
            return isValidated;
        }
    }
}