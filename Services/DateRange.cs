﻿using static BookingApi.Services.GalleryService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using BookingApi.Models;
using Azure.Core;
using Microsoft.EntityFrameworkCore;
using BookingApi.Data;

namespace BookingApi.Services
{
    public interface IRange
    {
        DateTime Start { get; }
        DateTime End { get; }
        bool Includes(DateTime value);
        bool Includes(IRange range);
    }

    public class DateRange : IRange
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public DateRange(DateTime start, DateTime end)
        {
            Start = start;
            End = end;
        }

        public bool Includes(DateTime value)
        {
            return (Start <= value) && (value <= End);
        }

        public bool Includes(IRange range)
        {
            return (Start <= range.Start) && (range.End <= End);
        }
    }

}