﻿using BookingApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BookingApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {

        //[HttpGet]
        ////[Authorize(Roles = "Administrator")]
        //public IActionResult Endpoint()
        //{
        //    var currentUser = GetCurrentUser();

        //    return Ok(currentUser);
        //}

        //[HttpGet("Admins")]
        ////[Authorize(Roles = "Administrator")]
        //public IActionResult AdminsEndpoint()
        //{
        //    var currentUser = GetCurrentUser();

        //    return Ok($"Hi {currentUser.GivenName}, you are an {currentUser.Role}");
        //}

        //[HttpGet("Sellers")]
        ////[Authorize(Roles = "Seller")]
        //public IActionResult SellersEndpoint()
        //{
        //    var currentUser = GetCurrentUser();

        //    return Ok($"Hi {currentUser.GivenName}, you are a {currentUser.Role}");
        //}

        //[HttpGet("AdminsAndSellers")]
        ////[Authorize(Roles = "Administrator,Seller")]
        //public IActionResult AdminsAndSellersEndpoint()
        //{
        //    var currentUser = GetCurrentUser();

        //    return Ok($"Hi {currentUser.GivenName}, you are an {currentUser.Role}");
        //}

        //[HttpGet("Public")]
        //public IActionResult Public()
        //{
        //    return Ok("Hi, you're on public property");
        //}

        //private User GetCurrentUser()
        //{
        //    var identity = HttpContext.User.Identity as ClaimsIdentity;

        //    if (identity != null)
        //    {
        //        var userClaims = identity.Claims;

        //        return new User
        //        {
        //            Username = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.NameIdentifier)?.Value,
        //            EmailAddress = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Email)?.Value,
        //            GivenName = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.GivenName)?.Value,
        //            Surname = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Surname)?.Value,
        //            Role = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Role)?.Value
        //        };
        //    }
        //    return null;
        //}
    }
}
