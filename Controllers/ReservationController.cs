using System;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using BookingApi.Models;
using BookingApi.Services;

namespace BookingApi.Controllers
{
    [ApiController]
    [Route("api/reservations")]
    public class ReservationController : ControllerBase
    {
        private readonly IReservationService _reservationService;
        
        public ReservationController(IReservationService reservationService)
        {
            _reservationService = reservationService;
        }

        [HttpGet]
        public IActionResult GetReservations([FromQuery] int estateId)
        {
            try
            {
                var reservations = _reservationService.GetReservationsByEstateId(estateId);
                return Ok(reservations);
            }
            catch (ApplicationException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult CreateReservation([FromBody] Reservation reservation)
        {
            var validatedstart = _reservationService.checkValidatedDate(reservation.estateId, reservation.start);
            var validatedend = _reservationService.checkValidatedDate(reservation.estateId, reservation.end);
            if (!validatedstart || !validatedend)
            {
                return BadRequest("Reservation date not available !");
            }

            try
            {
                _reservationService.CreateReservation(reservation);
                return Ok();
            }
            catch (ApplicationException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetReservationById(int id)
        {
            try
            {
                var reservations = _reservationService.GetReservationById(id);
                return Ok(reservations);
            }
            catch (ApplicationException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("search/{title}")]
        public IActionResult SearchReservations(string title)
        {
            try
            {
                var reservations = _reservationService.SearchReservations(title);
                return Ok(reservations);
            }
            catch (ApplicationException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}