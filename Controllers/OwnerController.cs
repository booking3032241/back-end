﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BookingApi.Models;
using BookingApi.Data;
using BookingApi.Services;

namespace BookingApi.Controllers
{
    [Route("api/owners")]
    [ApiController]
    public class OwnerController : ControllerBase
    {

        private readonly ApplicationDbContext _context;
        private readonly IOwnerService _ownerService;

        public OwnerController(ApplicationDbContext context, IOwnerService ownerService)
        {
            _context = context;
            _ownerService = ownerService;
        }

        [HttpGet]
        public async Task<IEnumerable<Owner>> Get()
        {
            return await _context.Owners.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            if (id < 1)
                return BadRequest();
            var Owner = await _ownerService.GetOwner( id);
            if (Owner == null)
                return NotFound();
            return Ok(Owner);

        }
        [HttpGet("user/{userId}")]
        public async Task<IActionResult> GetByUser(int userId)
        {
            if (userId < 1)
                return BadRequest();
            var Owner = await _ownerService.GetOwnerByUser( userId);
            if (Owner == null)
                return NotFound();
            return Ok(Owner);

        }
        [HttpPost]
        public async Task<IActionResult> Post(Owner Owner)
        {
            _context.Add(Owner);
            await _context.SaveChangesAsync();
            return Ok();
        }

        [HttpPatch]
        public async Task<IActionResult> Patch(Owner OwnerData)
        {
            if (OwnerData == null || OwnerData.id == 0)
                return BadRequest();

            var Owner = await _context.Owners.FindAsync(OwnerData.id);
            if (Owner == null)
                return NotFound();
            Owner.firstname     = OwnerData.firstname;
            Owner.lastname      = OwnerData.lastname;
            Owner.email         = OwnerData.email;
            Owner.phone         = OwnerData.phone;
            Owner.address       = OwnerData.address;
            Owner.city          = OwnerData.city;
            Owner.state         = OwnerData.state;
            Owner.zipcode       = OwnerData.zipcode;
            Owner.description   = OwnerData.description;
            //Owner.birthDate     = OwnerData.birthDate;


            await _context.SaveChangesAsync();
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id < 1)
                return BadRequest();
            var Owner = await _context.Owners.FindAsync(id);
            if (Owner == null)
                return NotFound();
            _context.Owners.Remove(Owner);
            await _context.SaveChangesAsync();
            return Ok();

        }


    }
}
