﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BookingApi.Models;
using BookingApi.Data;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.StaticFiles;
using Azure;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.Security.Claims;
using Serilog;
using Serilog.Core;
using Microsoft.Extensions.Logging;
using BookingApi.Services;

namespace BookingApi.Controllers
{
    [Route("api/galleries")]
    [ApiController]
    public class GalleryController : ControllerBase
    {

        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly IGalleryService _galleryService;

        public GalleryController(ApplicationDbContext context, IWebHostEnvironment hostEnvironment, IGalleryService galleryService)
        {
            _context = context;
            this._hostEnvironment = hostEnvironment;
            _galleryService = galleryService;
        }

        [HttpGet]
        public  List<Gallery>  Get([FromQuery] int entityId, int entityIdentifier)
        {
            return  _galleryService.GetAllGallery(entityId, entityIdentifier);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetOne(int id)
        {
            if (id < 1)
                return BadRequest();
            var Gallery = await _context.Galleries.FirstOrDefaultAsync(m => m.id == id);
            if (Gallery == null)
                return NotFound();

            return Ok(Gallery);

        } 

        [HttpGet]
        [Route("default")]

        public async Task<IActionResult> GetDefaultImage([FromQuery] int entityId, int entityIdentifier)
        {
            var imageName = string.Empty;
            var imageSrc = string.Empty; 

            if (_context is not null)
            {
                var Gallery = await _context.Galleries.FirstOrDefaultAsync(x => x.entityId == entityId & x.entityIdentifier == entityIdentifier);


                if (Gallery != null)
                {
                    imageName = Gallery.imageName;

                    imageSrc = String.Format("{0}://{1}{2}/Images/{3}", Request.Scheme, Request.Host, Request.PathBase, imageName);

                    Gallery.imageSrc = imageSrc;

                    return Ok(Gallery);
                }

            }
            return BadRequest();
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromForm] Gallery Gallery)
        {
            // Log.Information("rrrrrrrrrrrr {@Gallery} \r", Gallery);
            if (Gallery.imageFile == null || Gallery.imageFile?.Length == 0)
            {
                return StatusCode(400, "File not found !");
            }
            
            Gallery.imageName = await SaveImage(Gallery.imageFile);
            _context.Galleries.Add(Gallery);
            await _context.SaveChangesAsync();
            
            return StatusCode(201);
        }

        [HttpPatch]
        public async Task<IActionResult> Patch([FromForm] Gallery GalleryData)
        {
            if (GalleryData == null || GalleryData.id == 0)
                return BadRequest();

            var Gallery = await _context.Galleries.FindAsync(GalleryData.id);
            if (Gallery == null)
                return NotFound();
            Gallery.description = GalleryData.description;
            Gallery.imageName = await SaveImage(GalleryData.imageFile);

            await _context.SaveChangesAsync();
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id < 1)
                return BadRequest();
            var Gallery = await _context.Galleries.FindAsync(id);
            if (Gallery == null)
                return NotFound();
            DeleteImage(Gallery.imageName);
            _context.Galleries.Remove(Gallery);
            await _context.SaveChangesAsync();
            return Ok();

        }

        [HttpGet]
        [Route("DownloadFile/{id}")]
        public async Task<IActionResult> DownloadFile(int id)
        {
            var Gallery = await _context.Galleries.FirstOrDefaultAsync(m => m.id == id);
            var filename = Gallery?.imageName;
            var path = Path.Combine(Directory.GetCurrentDirectory(), "Images", filename);
            var stream = new FileStream(path, FileMode.Open);
            return File(stream, "application/octet-stream", filename);
        }

        [NonAction]
        public async Task<string> SaveImage(IFormFile imageFile)
        {
            string imageName = new String(Path.GetFileNameWithoutExtension(imageFile.FileName).Take(10).ToArray()).Replace(' ', '-');
            imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(imageFile.FileName);
            var imagePath = Path.Combine(_hostEnvironment.ContentRootPath, "Images", imageName);
            using (var fileStream = new FileStream(imagePath, FileMode.Create))
            {
                await imageFile.CopyToAsync(fileStream);
            }
            return imageName;
        }

        [NonAction]
        public void DeleteImage(string imageName)
        {
            var imagePath = Path.Combine(_hostEnvironment.ContentRootPath, "Images", imageName);
            if (System.IO.File.Exists(imagePath))
                System.IO.File.Delete(imagePath);
        }

        [NonAction]

        public string GetImageSrc(int entityId, int entityIdentifier)
        {
            var imageName = string.Empty;
            var imageSrc = string.Empty;

            if (_context is not null)
            {
                imageName = _context.Galleries.FirstOrDefault(x => x.entityId == entityId && x.entityIdentifier == entityIdentifier)?.imageName;
                imageSrc = String.Format("{0}://{1}{2}/Images/{3}", Request.Scheme, Request.Host, Request.PathBase, imageName);

            }
            return imageSrc;
        }
    }
}
