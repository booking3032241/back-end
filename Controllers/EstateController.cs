﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BookingApi.Models;
using BookingApi.Data;
using Microsoft.AspNetCore.Identity;
using BookingApi.Services;
using System.Collections.Generic;
using System.Collections;
using System.Reflection;
using System;
using System.Text.RegularExpressions;

namespace BookingApi.Controllers
{
    [Route("api/estates")]
    [ApiController]
    public class EstateController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly IGalleryService _galleryService;
        private readonly IEstateService _estateService;

   
        public EstateController(ApplicationDbContext context, IWebHostEnvironment hostEnvironment, IGalleryService galleryService, IEstateService estateService)
        {
            _context = context;
            this._hostEnvironment = hostEnvironment;
            _galleryService = galleryService;
            _estateService = estateService;
        }

        [HttpGet]
        public async Task<List<Estate>> Get()
        {
            return await _estateService.GetAllEstate();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            if (id < 1)
                return BadRequest();
            var Estate = await _estateService.GetEstate(id);
            if (Estate == null)
                return NotFound();
            return Ok(Estate);

        }

        [HttpGet]
        [Route("search")]
        public async Task<List<Estate>> Search([FromQuery]string? name, int nature, int ptype, string? city, int ownerId)
        {
            IQueryable<Estate> query = _context.Estates;

            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(e => e.name.Contains(name));
            }

           if (ownerId > 0)
           {
               query = query.Where(e => e.ownerId == ownerId);
           }

            if (nature > 0)
            {
                query = query.Where(e => e.nature == nature);
            }

            if (ptype > 0)
           {
               query = query.Where(e => e.ptype == ptype);
           } 
           
            if (!string.IsNullOrEmpty(city))
            {
                query = query.Where(e => e.city.Contains(city));
            }
            var data = await query.ToListAsync();
            var list = new List<Estate>();
            if (data?.Any() == true)
            {
                foreach (var item in data)
                {
                    list.Add(new Estate()
                    {
                        id = item.id,
                        name = item.name,
                        description = item.description,
                        nature = item.nature,
                        ptype = item.ptype,
                        price = item.price,
                        space = item.space,
                        bedrooms = item.bedrooms,
                        bathrooms = item.bathrooms,
                        garages = item.garages,
                        address = item.address,
                        country = item.country,
                        city = item.city,
                        state = item.state,
                        zipcode = item.zipcode,
                        latitude = item.latitude,
                        longitude = item.longitude,
                        defaultImageSrc = _galleryService.GetDefaultImageSrc(2, item.id),
                        galleries = _galleryService.GetAllGallery(2, item.id)
                    }
                    ); ;
                }
            }
            return list;
        }

        [HttpPost]
        public async Task<IActionResult> Post( Estate Estate)
        {
            _context.Estates.Add(Estate);
            await _context.SaveChangesAsync();

            return StatusCode(201);
        }

        [HttpPatch]
        public async Task<IActionResult> Patch( Estate EstateData)
        {
            if (EstateData == null || EstateData.id == 0)
                return BadRequest();

            var Estate = await _context.Estates.FindAsync(EstateData.id);
            if (Estate == null)
                return NotFound();
            Estate.name = EstateData.name;
            Estate.description = EstateData.description;
            Estate.name = EstateData.name;
            Estate.description = EstateData.description;
            Estate.nature = EstateData.nature;
            Estate.ptype = EstateData.ptype;
            Estate.price = EstateData.price;
            Estate.space = EstateData.space;
            Estate.bedrooms = EstateData.bedrooms;
            Estate.bathrooms = EstateData.bathrooms;
            Estate.garages = EstateData.garages;
            Estate.address = EstateData.address;
            Estate.country = EstateData.country;
            Estate.city = EstateData.city;
            Estate.state = EstateData.state;
            Estate.zipcode = EstateData.zipcode;
            Estate.latitude = EstateData.latitude;
            Estate.longitude = EstateData.longitude;


            await _context.SaveChangesAsync();
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id < 1)
                return BadRequest();
            var Estate = await _context.Estates.FindAsync(id);
            if (Estate == null)
                return NotFound();
            _context.Estates.Remove(Estate);
            await _context.SaveChangesAsync();
            return Ok();

        }
    }
}
