﻿using Azure.Core;
using BookingApi.Data;
using BookingApi.Models;
using BookingApi.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BookingApi.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        public static User user = new User();
        public static Owner owner = new Owner();


        private readonly IConfiguration _configuration;
        private readonly ApplicationDbContext _context;
        private readonly IGalleryService _galleryService;

        public LoginController(ApplicationDbContext context, IConfiguration configuration, IGalleryService galleryService)
        {
            _configuration = configuration;
            _context = context;
            _galleryService = galleryService;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(Register register)
        {
            var userData = _context.Users.FirstOrDefault(o => o.username == register.username);
            if (userData != null)
            {
                return BadRequest("User already exists!");
            }

            string passwordHash = BCrypt.Net.BCrypt.HashPassword(register.password);
            var user = new User
            {
                username = register.username,
                password = passwordHash,
                email = register.email,
                role = register.role
            };

            _context.Add(user);
            await _context.SaveChangesAsync();


            if (user != null)
            {
                var owner = new Owner
                {
                    userId = user.id,
                    firstname = register.firstname,
                    lastname = register.lastname,
                    email = register.email,
                    phone = register.phone,
                    birthDate = register.birthDate,
            };

                _context.Add(owner);
                await _context.SaveChangesAsync();

                var token = CreateToken(user);

                user.defaultImageSrc = _galleryService.GetDefaultImageSrc(1, user.id);

                var result = new Dictionary<string, dynamic> { { "user", user }, { "token", token } };

                return Ok(result);
            }

            return NotFound("User not found");
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public IActionResult Login([FromBody] Login login)
        {
            var user = Authenticate(login);

            if (user != null)
            {
                if (!BCrypt.Net.BCrypt.Verify(login.password, user.password))
                {
                    return BadRequest("Wrong password.");
                }
                var token = CreateToken(user);

                user.defaultImageSrc = _galleryService.GetDefaultImageSrc(1, user.id);

                var result = new Dictionary<string, dynamic> { { "user", user }, { "token", token } };

                return Ok(result);
            }

            return NotFound("User not found");
        }

        private string CreateToken(User user)
        {
            List<Claim> claims = new List<Claim> {
                new Claim(ClaimTypes.Name, user.username),
                new Claim(ClaimTypes.Role, "Admin"),
                new Claim(ClaimTypes.Role, "User"),
            };
            Console.Write(_configuration.GetSection("AppSettings:Token").Value!);

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetSection("AppSettings:Token").Value!));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var token = new JwtSecurityToken(
                    claims: claims,
                    expires: DateTime.Now.AddDays(1),
                    signingCredentials: creds
                );

            var jwt = new JwtSecurityTokenHandler().WriteToken(token);

            return jwt;
        }

        private User Authenticate(Login login)
        {
            var currentUser = _context.Users.FirstOrDefault(o => o.username.ToLower() == login.username.ToLower());


            if (currentUser != null)
            {
                return currentUser;
            }

            return null;
        }
    }
}
